# Deplutils
Deployment utility scripts

Written bespokely for managing servers maintained by Driv Digital

The purpose of this project is to easily install and maintain server software

One line installation:

	sh -c "$(curl -fsSL https://bitbucket.org/drivdigital/deplutils/raw/master/installer.sh)"

Since the installation includes oh-my-zsh and the shell is changed then the following command must also be run:

`/root/deplutils/deplutils install`

## Setting up a new Node

Visit the [Linode Manager](https://manager.linode.com/linodes) to create a new Node. A node is a separate *paid* instance of a server that we can use for clients.

#### Step by step guide for creating a node
- Clidk 'Create new Node'
- Choose 'Deploy an image'
- Select 'Ubuntu 17.10'
	- Reduce deployment disk size by '2gb'
	- Set Swap Disk to '512mb'
	- Set root password, make sure this is secure
- Edit swap disk
	- Increase swap size by 2gb
- Boot linode
- Visit **Remove Access** tab to see **SSH details**

## Setting up the server

Once the Node is created, you can set it up with our default settings:

#### Step by step guide for installing server software

- SSH into the server with the root password
- Paste the one line installer and run:

	sh -c "$(curl -fsSL https://bitbucket.org/drivdigital/deplutils/raw/master/installer.sh)"


- Once composer is installed, run `/root/deplutils/deplutils install`
	- Give the server a name, only use one word, no hyphens, spaces etc.
	- Respond with YES, this is a server installation
- Incrementally install each server script
	- Run `./deplutils script:all`
	- When prompted, add the SSH-KEY for the server to the [drivdigital ssh keys](https://bitbucket.org/account/user/drivdigital/ssh-keys/)


```
If the /www-folder was not created, you might need
to run `deplutils scrips:run scripts/20170725_163000_install-www-tools` again.
```

#### Final step: Crontab

Edit the crontab using:

```
crontab -e
```

Copy paste the following cron configurations:

```
0 * * * * crontab -l > /root/crontab.txt
*/10 * * * * /usr/local/bin/nginx -t 2> /root/nginx-test.txt
0 0 * * * letsencrypt renew >> /var/log/letsencrypt-cron.log 2>&1
* * * * * /root/deplutils/deplutils cron
0 1 * * * service nginx restart
```