#!/bin/bash

NGINX_VERSION=$(nginx -v 2>&1)
if [[ $NGINX_VERSION == *"1.16.0"* ]]; then
  exit 1
fi

service stop nginx

# Based on instructions from modpagespeed
# https://www.modpagespeed.com/doc/build_ngx_pagespeed_from_source
apt-get install build-essential zlib1g-dev libpcre3 libpcre3-dev unzip uuid-dev gcc-7 -y
apt-get install libssl-dev libxslt-dev libgd-dev libgeoip-dev -y

NPS_VERSION=1.13.35.2-stable
cd
wget https://github.com/apache/incubator-pagespeed-ngx/archive/v${NPS_VERSION}.zip
unzip v${NPS_VERSION}.zip
nps_dir=$(find . -name "*pagespeed-ngx-${NPS_VERSION}" -type d)
cd "$nps_dir"
NPS_RELEASE_NUMBER=${NPS_VERSION/beta/}
NPS_RELEASE_NUMBER=${NPS_VERSION/stable/}
psol_url=https://dl.google.com/dl/page-speed/psol/${NPS_RELEASE_NUMBER}.tar.gz
[ -e scripts/format_binary_url.sh ] && psol_url=$(scripts/format_binary_url.sh PSOL_BINARY_URL)
wget ${psol_url}
tar -xzvf $(basename ${psol_url})

NGINX_VERSION=1.16.0
cd && \
wget http://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz && \
tar -xvzf nginx-${NGINX_VERSION}.tar.gz && \
cd nginx-${NGINX_VERSION}/ && \
./configure --add-module=$HOME/$nps_dir \
    --with-ld-opt='-Wl,-Bsymbolic-functions -Wl,-z,relro -Wl,-z,now -fPIC' \
    --with-cc-opt='-Wformat -Werror=format-security -Wno-cast-function-type' \
    --prefix=/usr/share/nginx \
    --conf-path=/etc/nginx/nginx.conf \
    --http-log-path=/var/log/nginx/access.log \
    --error-log-path=/var/log/nginx/error.log \
    --lock-path=/var/lock/nginx.lock \
    --pid-path=/run/nginx.pid \
    --modules-path=/usr/lib/nginx/modules \
    --http-client-body-temp-path=/var/lib/nginx/body \
    --http-fastcgi-temp-path=/var/lib/nginx/fastcgi \
    --http-proxy-temp-path=/var/lib/nginx/proxy \
    --http-scgi-temp-path=/var/lib/nginx/scgi \
    --http-uwsgi-temp-path=/var/lib/nginx/uwsgi \
    --with-pcre-jit \
    --with-http_ssl_module \
    --with-http_stub_status_module \
    --with-http_realip_module \
    --with-http_auth_request_module \
    --with-http_v2_module \
    --with-http_dav_module \
    --with-http_slice_module \
    --with-threads \
    --with-http_addition_module \
    --with-http_geoip_module=dynamic \
    --with-http_gunzip_module \
    --with-http_gzip_static_module \
    --with-http_image_filter_module=dynamic \
    --with-http_sub_module \
    --with-http_xslt_module=dynamic \
    --with-stream=dynamic \
    --with-stream_ssl_module \
    --with-mail=dynamic \
    --with-mail_ssl_module && \
make  && \
make install

# Remove the ubuntu installed binary or old symlink
rm /usr/sbin/nginx

# Symlink the new nginx binary
ln -s /usr/share/nginx/sbin/nginx /usr/local/bin/nginx
ln -s /usr/share/nginx/sbin/nginx /usr/sbin/nginx


