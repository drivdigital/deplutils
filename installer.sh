#!/bin/sh
# Install git and zsh
sudo apt install -y zsh git zip php7.2 php7.2-mbstring php7.2-xml php7.2-mysql php7.2-curl php7.2-fpm php7.2-gd


if [ ! -d ~/.oh-my-zsh ]; then
  echo "Installing Oh my ZSH" && \
  # Download oh my zsh
  curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh > /tmp/oh-my-zsh-install.sh && \
  # Remove the last step that engages zsh
  sed -i 's/env zsh//g' /tmp/oh-my-zsh-install.sh && \
  # Execute the installer
  sh /tmp/oh-my-zsh-install.sh && \
  # Delete the installer after completion
  rm /tmp/oh-my-zsh-install.sh && \
  # Set the zsh-theme to "gianu"
  sed -i 's/ZSH_THEME="robbyrussell"/ZSH_THEME="gianu"/g' .zshrc
fi

if [ ! -d /root/deplutils ]; then
  # Clone the deplutils repo
  sudo git clone https://bitbucket.org/drivdigital/deplutils.git /root/deplutils
fi

# Check for deplutils
if [ -d /root/deplutils ]; then
  cd /root/deplutils
  # Install composer
  if [ ! -f /usr/local/bin/composer ]; then
    /root/deplutils/composer-installer.sh && \
    sudo mv composer.phar /usr/local/bin/composer
  fi
  # Composer install deplutils
  if [ -f /usr/local/bin/composer ]; then
    composer install && \
    # .. and add it to .zshrc
    echo "alias deplutils=/root/deplutils/deplutils" >> ~/.zshrc
  else
    echo "Failure! Composer was not installed!"
  fi

  echo ""
  echo "deplutils has now been downloaded, and requirements have been installed."
  echo "To complete installation run 'deplutils install'."

  # Reintroduce the step removed from oh-my-zsh installer
  env zsh
else
  echo "Failure! The /root/deplutils does not exist!"
fi
