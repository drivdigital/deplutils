<?php

namespace App\Command;

use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ScriptsRun extends Command
{
  protected function configure()
  {
    $this
        ->setName('scripts:run')
        ->setDescription('Run a script.')
        ->setDefinition(
            new InputDefinition( [
                new InputOption('file', 'f', InputOption::VALUE_REQUIRED),
                new InputArgument( 'file', InputOption::VALUE_REQUIRED ),
            ] )
        )
        ;
  }

  protected function execute(InputInterface $input, OutputInterface $output)
  {
      $io = new SymfonyStyle( $input, $output );
      $file = $input->getArgument( 'file' );
      // $io->confirm( "How's the day going?" );
      $files = glob( 'scripts/*' );
      $io->listing( $files );
      $io->title( $file );
      if ( ! $file ) {
        throw new \Exception("Missing argument: --file|f", 1);
      }
      $script = new \App\Script($file);
      $script->execute();
  }
}
