<?php

namespace App\Command;

use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class SelfUpdate extends Command
{
  protected function configure()
  {
    $this
        ->setName('self-update')
        ->setDescription('Update deplutils')
        ;
  }

  protected function execute(InputInterface $input, OutputInterface $output)
  {
      $io = new SymfonyStyle( $input, $output );
      chdir( BASE_DIR );
      if ( $io->confirm( 'Please confirm to run "git fetch --all && git reset --hard origin/master"' ) ) {
        passthru( 'git fetch --all && git reset --hard origin/master' );
      }
  }
}
