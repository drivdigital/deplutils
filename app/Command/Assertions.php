<?php

namespace App\Command;

use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class Assertions extends Command
{
    protected function configure()
    {
        $this
            ->setName('assertions')
            ->setDescription('Check assertions.')
            ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io    = new SymfonyStyle($input, $output);
        $root  = dirname(dirname(__DIR__));
        $files = glob("$root/assertions/**/*.php");
        foreach ($files as $file) {
            try {
                require_once $file;
            }
            catch (\Exception $e) {
                $io->warning($e->getMessage());
            }
        }
    }
}
