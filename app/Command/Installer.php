<?php

namespace App\Command;

use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class Installer extends Command
{
  protected function configure()
  {
    $this
        ->setName('install')
        ->setDescription('Install and configure deplutils')
        ;
  }

  protected function execute(InputInterface $input, OutputInterface $output)
  {
      $io = new SymfonyStyle( $input, $output );
      if ( $this->install_config_file( $io ) && $io->confirm( 'Is this a server installation?' ) ) {
        $this->install_crontab( $io );
      }
  }

  protected function install_config_file( $io ) {
    chdir( BASE_DIR );
    // Check for prior installation
    if ( file_exists( '.deplutils-config.json' ) ) {
      $io->warning( 'deplutils has already been installed. Delete .deplutils-config.json and rerun this command to reinstall' );
      return false;
    }
    $config = [];
    // Ask for a name
    $config['name'] = $io->ask( 'Give this server a name:' );
    if ( ! $config['name'] ) {
      $io->warning( 'The server must have a name to install deplutils' );
    }
    // Write the new config file
    file_put_contents( '.deplutils-config.json', json_encode( $config, JSON_PRETTY_PRINT ) );
    // Verify the config
    if ( ! file_exists( '.deplutils-config.json' ) ) {
      $io->error( 'The installer attempted to save to .deplutils-config.json, but failed' );
      return false;
    }
    return true;
  }

  protected function install_crontab( $io ) {
    $crontab = shell_exec( 'crontab -l 2>/dev/null' );
    $command = BASE_DIR .'/deplutils cron';
    $tab = sprintf( "* * * * * %s", $command );
    // Check for installation
    if ( false !== strpos( $crontab, $tab ) ) {
      $io->comment( "Crontab for deplutils has already been installed, skipping." );
      return false;
    }
    // Install crontab
    $io->comment( "Installing crontab task." );
    shell_exec( "echo '$crontab\n$tab' | crontab - " );

    // Verify installation
    $crontab = shell_exec( 'crontab -l 2>/dev/null' );
    if ( false === strpos( $crontab, $tab ) ) {
      $io->error( "Crontab was not installed despite the best effor of this installer!" );
      return false;
    }
    return true;
  }
}
