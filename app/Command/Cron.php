<?php

namespace App\Command;

use Exception;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class Cron extends Command
{
  protected function configure()
  {
    $this
        ->setName('cron')
        ->setDescription('Scheduled tasks for deplutils')
        ;
  }

  protected function execute(InputInterface $input, OutputInterface $output)
  {
      $io = new SymfonyStyle( $input, $output );
      // echo "Nothing to do\n"; // Usually don't output because it will spam the cron.log
  }
}
