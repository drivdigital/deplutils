<?php

namespace App\Command;

use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ScriptsAll extends Command
{
    protected function configure()
    {
        $this->setName('scripts:all')
            ->setDescription('Run all scripts.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $path = dirname(dirname(__DIR__)).'/scripts';
        // $io->comment($path);
        $files = glob("$path/*");
        foreach ($files as $file) {
            $basename = basename($file);
            if (! preg_match('/^\d+_\d+_/', $basename)) {
                continue;
            }
            $script = new \App\Script($file);
            try {
                $script->execute();
            } catch (\Exception $e) {
                echo $e->getMessage(). ". Skipping $basename.\n";
            }
        }

    }
}
