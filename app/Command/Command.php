<?php

namespace App\Command;

use Exception;
use Symfony\Component\Console\Command\Command as BaseCommand;

abstract class Command extends BaseCommand
{
  /**
   * Config
   * @param  string       $key
   * @return bool|string       Returns the config variable on success or false on failure
   */
  protected function config( $key ) {

    return false;
  }
}
