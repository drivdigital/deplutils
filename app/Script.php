<?php

namespace App;

class Script
{
    protected $file;
    public function __construct($file)
    {
      if ( ! file_exists( $file ) ) {
        throw new \Exception("File, $file, does not exist", 1);
      }
      $file = realpath( $file );
      if ( ! preg_match( '/\/scripts\/[\/\dA-Za-z_\-]+$/', $file ) ) {
        throw new \Exception("File, $file, must be inside a scripts folder and have no extension", 1);
      }
      if ( ! is_executable( $file ) ) {
        throw new \Exception("File, $file, is not executable", 1);
      }
      $this->file = $file;
    }

    public function execute($force = false)
    {
        $basename = basename($this->file);
        if (! $force && $this->hasExecuted()) {
            throw new \Exception("The script has already been executed");
        }
        if (`which system_profiler`) {
            // Mac OS X
            echo "$this->file\n";
        } else {
            // LINUX
            passthru( $this->file );
        }
        $path = dirname($this->file);
        file_put_contents(
            "$path/.execution_history",
            '['. date('Y-m-d H:i:s') ."] $basename\n",
            FILE_APPEND
        );
    }

    public function hasExecuted()
    {
        $basename = basename($this->file);
        $path     = dirname($this->file);
        $content  = '';
        if (file_exists("$path/.execution_history")) {
            $content = file_get_contents("$path/.execution_history");
        }
        return strpos($content, $basename) !== false;
    }
}