<?php
use Webmozart\Assert\Assert;

Assert::fileExists('/etc/nginx/snippets/global-server.conf', 'Nginx snippets has not been set up correctly. Please run `deplutils scripts:run scripts/20190827_154105_nginx_snippets`');
Assert::fileExists('/etc/nginx/snippets/global-server-cached.conf');
Assert::fileExists('/etc/nginx/snippets/website.conf');
Assert::fileExists('/etc/nginx/snippets/pagespeed-defaults.conf');

Assert::true(is_link('/etc/nginx/nginx.conf'), 'Nginx configuration file should be a symlink of /root/deplutils/assets/nginx/nginx.conf');
