<?php
use Webmozart\Assert\Assert;

Assert::fileExists('/etc/redis/redis.conf');
$content = file_get_contents('/etc/redis/redis.conf');
Assert::notContains($content, '# requirepass', 'Looks like redis is insecure! Please see scripts/20190819_160600_install_redis');
Assert::contains($content, 'requirepass', 'Redis is insecure! Please see scripts/20190819_160600_install_redis');
Assert::notContains($content, 'supervised no', 'Looks like redis is not managed by supervised! Please see scripts/20190819_160600_install_redis');
Assert::contains($content, 'supervised systemd', 'Redis is not managed by supervised! Please see scripts/20190819_160600_install_redis');
