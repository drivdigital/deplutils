<?php
use Webmozart\Assert\Assert;

$content = file_get_contents('/root/nginx-test.txt');

Assert::contains($content, 'syntax is ok', 'There is a syntax problem with nginx configuration. See `sudo nginx -t` for more information.');
Assert::contains($content, 'test is successful', 'Testing the nginx configuration was not successful. See `sudo nginx -t` for more information.');

$content = shell_exec('grep -R "include global-server.conf" /etc/nginx 2>/dev/null');
$lines = explode("\n", $content);

foreach ($lines as $line) {
    if (empty($line)) {
        continue;
    }
    Assert::startsWith($line, '/etc/nginx/snippets', "One or more website config files are still using \"include global-server.conf\". See: $line. The \"/etc/nginx/global-server.conf\" has been depreceated. Configurations should now use either \"/etc/nginx/snippets/website.conf\" or \"/etc/nginx/snippets/global-server.conf\"");
}

$content = shell_exec('nginx -v 2>&1');
Assert::notEmpty(preg_match('/(\d+.\d+.\d+)/', $content, $matches), 'Could not determine the nginx version number. Please check the version command in deplutils/assertions/configuration/nginx.php.');
$version = $matches[1];
Assert::greaterThanEq(
    version_compare($version, '1.16.0'),
    0,
    'Nginx version is outdated. Please run deplutils/scripts/20190813_111100_reinstall_pagespeed_nginx.'
);
