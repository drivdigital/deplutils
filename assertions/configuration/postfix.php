<?php
use Webmozart\Assert\Assert;

Assert::fileExists('/etc/postfix/main.cf');
$content = file_get_contents('/etc/postfix/main.cf');
Assert::contains($content, 'inet_protocols = ipv4', 'Postfix is not configured to use ipv4. Please run `deplutils scripts:run scripts/20190923_121100_configure_postfix`');
