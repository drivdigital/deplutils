<?php
use Webmozart\Assert\Assert;

// Check php-fpm port configuration.
Assert::fileExists('/etc/php/7.2/fpm/pool.d/www.conf');
$content = file_get_contents('/etc/php/7.2/fpm/pool.d/www.conf');
Assert::contains($content, 'listen = 127.0.0.1:9000');

// Assert php upload limits.
Assert::fileExists('/etc/php/7.2/fpm/php.ini');
$content = file_get_contents('/etc/php/7.2/fpm/php.ini');
Assert::eq(preg_match('/post_max_size = (\d+)M/', $content, $matches), 1, 'Could not determine post_max_size. See assertions/configuration/php-fpm.php. Please run `deplutils scripts:run scripts/20190923_113200_set_php_upload_limits`');
Assert::greaterThanEq($matches[1], 32, 'Post max size should be at least 32 Mb. See /etc/php/7.2/fpm/php.ini');
Assert::eq(preg_match('/upload_max_filesize = (\d+)M/', $content, $matches), 1, 'Could not determine upload_max_filesize. See assertions/configuration/php-fpm.php. Please run `deplutils scripts:run scripts/20190923_113200_set_php_upload_limits`');
Assert::greaterThanEq($matches[1], 16, 'Upload max size should be at least 16 Mb. See /etc/php/7.2/fpm/php.ini');
