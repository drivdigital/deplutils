<?php
use Webmozart\Assert\Assert;

$version = phpversion();

if (! preg_match('/^(\d.\d)/', $version, $matches)) {
	throw new Exception("Could not determine php version!");
}

$minor = $matches[1];

$safe = [
	'7.3' => '7.3.9',
	'7.2' => '7.2.22',
	'7.1' => '7.1.32',
];

Assert::notEmpty($safe[$minor], 'Could not determine php-7 version! Minor version was: '. $minor);
Assert::greaterThanEq(version_compare(phpversion(), $safe[$minor]), 0, "PHP version $version is not safe!");
