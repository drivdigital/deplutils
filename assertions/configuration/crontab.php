<?php
use Webmozart\Assert\Assert;

Assert::fileExists('/root/crontab.txt', 'The root crontab is not configured correctly. Please configure cron according to the deplutils readme.');

$content = file_get_contents('/root/crontab.txt');

Assert::contains($content, '0 * * * * crontab -l > /root/crontab.txt');
Assert::contains($content, '*/10 * * * * /usr/local/bin/nginx -t 2> /root/nginx-test.txt');
Assert::contains($content, '0 0 * * * letsencrypt renew >> /var/log/letsencrypt-cron.log 2>&1');
Assert::contains($content, '* * * * * /root/deplutils/deplutils cron');
Assert::contains($content, '0 1 * * * service nginx restart');