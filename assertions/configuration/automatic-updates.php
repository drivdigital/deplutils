<?php
use Webmozart\Assert\Assert;

Assert::fileExists('/etc/apt/apt.conf.d/20auto-upgrades');
Assert::fileExists('/etc/apt/apt.conf.d/50unattended-upgrades');


$compare = function ($f1, $f2) {
    Assert::true(md5_file($f1) === md5_file($f2), "File, $f1 and $f2, should match. Please run deplutils/scripts/20171110_095500_enable_unattended_upgrades.");
};

$compare('/etc/apt/apt.conf.d/20auto-upgrades', '/root/deplutils/assets/20auto-upgrades');
$compare('/etc/apt/apt.conf.d/50unattended-upgrades', '/root/deplutils/assets/50unattended-upgrades');
